# Ancile Networking
### About
https://gitlab.com/misc-scripts1/windows/ancile/AncilePlugin_Networking

Ancile Networking block unwanted hosts and domains using the Windows hosts file, routing table, and firewall

This is a plugin that requires Ancile_Core (https://gitlab.com/misc-scripts1/windows/ancile/Ancile_Core) 

For more information go to https://voat.co/v/ancile